# Too Much Sound
## Do you think that python is not noisy enough?

A friend has migrated from EES to python (with scipy) to run some system solving. He told me that he regret the sound that EES did in case of success and error.

Life if too short for such regret. No more.

I introduce you Too Much Sound. It will alert you with a classy sound in case of unexpected error or not-less-unexpected success. It come in two flavor : fancy decorator or nice context manager.

You can install it easily (using pip with that repository)

```bash
pip install git+https://gitlab.com/celliern/too_much_sound
```

It's simple to use :

```python
import time

def I_am_error():
    time.sleep(.1)
    raise RuntimeError

# decorator
@alert_with_sound_and_style
def I_am_success():
    time.sleep(.1)

I_am_success()

# context manager
with sound_alert():
    I_am_error()
```

Have a nicer day!
