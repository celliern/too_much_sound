import tempfile
from functools import wraps, partial
from contextlib import contextmanager
import importlib.resources
from subprocess import DEVNULL

from pydub import AudioSegment
from pydub.playback import play, subprocess

from . import sounds

# dirty hack to silence pydub (ffplay is really verbose...)
subprocess.call = partial(subprocess.call, stdout=DEVNULL)

with tempfile.NamedTemporaryFile() as error_sound_file:
    error_sound_bin = importlib.resources.read_binary(sounds, "error.wav")
    error_sound_file.write(error_sound_bin)
    error_song = AudioSegment.from_wav(error_sound_file)


with tempfile.NamedTemporaryFile() as success_sound_file:
    success_sound_bin = importlib.resources.read_binary(sounds, "success.wav")
    success_sound_file.write(success_sound_bin)
    success_song = AudioSegment.from_wav(success_sound_file)

def alert_with_sound_and_style(function, on_error=True, on_success=True, exception_class=Exception):
    @wraps(function)
    def function_with_sound(*args, **kwargs):
        try:
            res = function(*args, **kwargs)
        except exception_class:
            if on_error:
                play(error_song)
            raise
        if on_success:
            play(success_song)
        return res

    return function_with_sound

@contextmanager
def sound_alert(on_error=True, on_success=True, exception_class=Exception):
    try:
        yield
    except exception_class:
        if on_error:
            play(error_song)
        raise
    if on_success:
        play(success_song)

if __name__ == "__main__":
    import time

    def I_am_error():
        time.sleep(.1)
        raise RuntimeError

    def I_am_success():
        time.sleep(.1)

    alert_with_sound_and_style(I_am_success)()
    with sound_alert():
        I_am_error()
